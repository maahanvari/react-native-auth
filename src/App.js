import React from 'react';
import { View } from 'react-native';
import firebase from 'firebase';
import { Header, Button, Spinner } from './component/common';
import LoginForm from "./component/LiginForm"; 


export default class App extends React.Component {
  state = { loggedIn: null };

  componentWillMount() {
     firebase.initializeApp({
        apiKey: 'AIzaSyB-kcrolPOM0LKi3WgNGtziT_6ewLgrMSw',
        authDomain: 'authentication-3116c.firebaseapp.com',
        databaseURL:'https://authentication-3116c.firebaseio.com',
        projectId: 'authentication-3116c',
        storageBucket: 'authentication-3116c.appspot.com',
        messagingSenderId: '325855790340'
    }); 
    firebase.auth().onAuthStateChanged((user) => {
      if ( user) {
        this.setState({ loggedIn : true });
      }else{
        this.setState({ loggedIn: false });
      }
    });

  } 
  
  renderContent() {

    switch (this.state.loggedIn) {
      case true:
        return( 
        <View style={{ padding : 5, flexDirection: 'row'}}>
        <Button onPress={() => firebase.auth().signOut()}>
          Log Out
        </Button>
    </View>);
      case false:
        return  <LoginForm />;
      default:
        return (
          <View style={{ padding : 5, flexDirection: 'row'}}>
            <Spinner size="large" />
          </View>);
    }
  }

  render() {
    return (
      <View >
      <Header HeaderText="Authentication" />
      {this.renderContent()}
      </View>
    );
  }
}


